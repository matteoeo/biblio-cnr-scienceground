import networkx as nx
from bokeh.io import show, output_file
from bokeh.models import Plot, Range1d, MultiLine, Circle, TapTool, OpenURL, HoverTool, CustomJS, Slider, Column, ColumnDataSource, WheelZoomTool
from bokeh.models.graphs import EdgesAndLinkedNodes
from bokeh.plotting import figure, from_networkx
from dask.dataframe.core import DataFrame
from bokeh.models.widgets import CheckboxGroup
import pandas as pd
from bokeh.layouts import row
import numpy as np
import matplotlib.pyplot as pl
from matplotlib import colors as mcolors

# first run links.py to produce the CSV datasets from the Zotero library
df = pd.read_csv("data/links.csv",sep=";")
nodes = pd.read_csv("data/nodes.csv",sep=";")
checknames = list(np.unique(df['edge_name']))
idx = [checknames.index(s) for s in df['edge_name']]
df ['checkbox'] = idx

# c = list(dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS).values())
c = ['black','#ff9300', '#919191','#941651', '#8df900','#521b92','#005392','#008e00', '#941100','#ff2f92', '#9437ff','#0096ff','#ff7d78', '#7980ff', '#72fcd5','black','#ff9300', '#919191','#941651', '#8df900','#521b92','#005392','#008e00', '#941100','#ff2f92', '#9437ff','#0096ff','#ff7d78', '#7980ff', '#72fcd5','black','#ff9300', '#919191','#941651', '#8df900','#521b92','#005392','#008e00', '#941100','#ff2f92', '#9437ff','#0096ff','#ff7d78', '#7980ff', '#72fcd5']

net_graph = nx.MultiGraph()
for _index,_row in nodes.iterrows():

    net_graph.add_node(_row['id'],title=_row['title'],author=_row['author'],isbn=_row['isbn'])

for _index,_row in df.iterrows():  
    cb = _row['checkbox'] 
    print(checknames[cb],c[cb])
    net_graph.add_edge(_row['idsource'], _row['idtarget'],checkbox=cb,
     edge_color=c[cb], edge_width=1)


graph_plot = Plot(plot_width = 700, plot_height = 700, x_range = Range1d(-1.1, 1.1), y_range = Range1d(-1.1, 1.1))

# print(nx.spring_layout(net_graph))
# nx.draw_networkx_edges(net_graph,pos=nx.spring_layout(net_graph))   
# nx.draw(net_graph)
# pl.show()



# graph_setup = from_networkx(net_graph, nx.spring_layout, scale = 1, center = (0, 0), k=1.0)
graph_setup = from_networkx(net_graph, nx.kamada_kawai_layout)#, scale = 1, center = (0, 0), k=1.0)
graph_setup.node_renderer.glyph = Circle(size = 15, fill_color = '#75d5ff',fill_alpha=0.6, line_alpha=0.2)
graph_setup.edge_renderer.glyph = MultiLine(line_color = 'edge_color', line_alpha = 0.2, line_width = 'edge_width')

node_hover_tool = HoverTool(tooltips = [("Title", "@title")
                , ("Author", "@author"),
                ("ISBN","@isbn")
                ], 
                )
graph_plot.add_tools(node_hover_tool)
graph_plot.add_tools(WheelZoomTool())

graph_plot.renderers.append(graph_setup)

# source  = ColumnDataSource(data=graph_setup.edge_renderer.data_source.data)

source = graph_setup.edge_renderer.data_source

code = ''' 
    var active = cb_obj.active
    var data = graph_setup.edge_renderer.data_source.data
    var checkbox = data['checkbox']
    var new_start = data['start'].slice();  
    var new_end = data['end'].slice();

    console.log(active);
    console.log(checkbox);
    console.log("CB len", checkbox.length)

    var    active_start = [];
    var    active_end = [];
    var widths = [];
    for (var i = 0; i < checkbox.length ; i++) {
        if (active.includes(checkbox[i])){
            data['edge_width'][i]=3;
            
        }   
        else{
        console.log("zeroing");
        data['edge_width'][i]=0.5;
        }
    
    }
    console.log(data);
    graph_setup.edge_renderer.data_source.data=data;   

    graph_setup.edge_renderer.data_source.  change.emit();

'''
output_file("../percorsi/html/networkx_graph.html")
# print((graph_setup.edge_renderer.data_source.data['checkbox'])) 

callback = CustomJS(args = dict(graph_setup = graph_setup), 
                                
                                code = code)
# slider = Slider(title = 'Slider', start = 1, end = 6, value = 6)
checkboxes = CheckboxGroup(labels=checknames, active=[])
checkboxes.js_on_change('active', callback)

layout = row(graph_plot, checkboxes)
show(layout)