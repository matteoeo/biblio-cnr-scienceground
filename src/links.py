import pandas as pd
import numpy as np
import myio

from pyzotero import zotero



library_id = '2536502'

api_key = '448IcF6k31mfLAEa7ywdsqaC'

library_type = 'group'

zot = zotero.Zotero(library_id, library_type, api_key)

items = zot.everything(zot.top())
# items = zot.top(limit=5)

library = myio.read_library(items)
print(library)

nodes = open("data/nodes.csv",'w')
with open("data/links.csv", 'w') as fout:
	nodes.write("id;title;author;isbn\n")
	print("idsource;idtarget;source;target;edge_name",file=fout)
	for i,ti in enumerate(library.keys()):
		# print(ti)
		authors	= library[ti]['authors']
		isbn = library[ti]['ISBN']
		nodes.write(f"{i};{ti};\"{authors}\";{isbn}\n")

		# print(i, ti)
		tagsi = library[ti]['tags']
		for j,tj in enumerate(library.keys()):
			if i !=j:
				tagsj = library[tj]['tags']
				for k in tagsi:
					if k in tagsj:
						fout.write(f"{i};{j};{ti};{tj};{k}\n")


