# Biblio CNR  Scienceground

Percorsi bibliografici comuni Scienceground-CNR.

## Come partecipare?

Il modo più semplice di partecipare alla Biblioteca è di unirsi al gruppo su Zotero

https://www.zotero.org/groups/2536502/scienceground-cnr/library

Si accede al gruppo in due passi:
1. creazione di un account su Zotero https://www.zotero.org/user/register
2. ricerca e aggiunta della libreria condivisa (*shared library*) Scienceground. Per poter modificare i contenuti, bisogna richiere l'accesso (e attendere la risposta). Per più informazioni sui gruppi in Zotero, vedere qui https://www.zotero.org/support/groups

Il modo più comodo di interagire con la libreria è di utilizzare il client. Questo può essere scaricato da qui https://www.zotero.org/download/

Nel client di Zotero, la libreria di gruppo si presenta così:

![interfaccia](fig/interface.png)


## Come modificare la libreria?

Il modo più semplice per contribuire è tramite due possibili azioni

1. L'aggiunta di un titolo interessante
2. L'assegnazione di parole chiave (**tag**) ai titoli esistenti.

### Aggiunta di un titolo

Il metodo raccomandato per aggiungere un titolo è tramite l'ISBN o l'ISSN:

1. Individuare il codice ISBN del volume di riferimento
![commedia](fig/commedia.png)
2. Cliccare sull'icona con una penna magica e un segno + e inserire il codice isbn
![isbn](fig/isbn.png)


### Tag

Le parole chiave in Zotero sono chiamate *tag*. Ce ne sono di due tipi
1. tag assegnati **automaticamente** al momento dell'aggiunta automatica di un titolo
2. tag assegnati **manualmente**.

`Importante` *La costruzione dei percorsi bibliografici è basata sui tag assegnati manualmente.*

#### Tag automatici
Per escludere/includere i tag automatici, cliccare sul pulsante colorato nella parte bassa del riquadro dei tag

![tagmenu](fig/tagmenu.png)

#### Aggiunta tag

Per aggiungere un tag ad un titolo, cliccare in alto a destra sul pulsante tag e in seguito su **Add**

![tagmenu](fig/addtag.png)

Per assegnare lo stesso tag a più titoli, selezionare i titoli nel pannello centrale e trascinare il contenuto sul tag desiderato in basso a sinistra.

![tagmenu](fig/drag.gif)
