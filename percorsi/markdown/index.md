# Biblioteca CNRS - Scienceground

## Anthropology and Ecology
- Déborah Danowski, Eduardo Batalha Viveiros de Castro,  **The ends of the world**, ISBN/ISSN: 978-1-5095-0397-1 978-1-5095-0398-8
-  Haraway Donna,  **Chthulucene. Sopravvivere su un pianeta infetto**, ISBN/ISSN: 
- Bruno Latour, Catherine Porter,  **Facing Gaia: eight lectures on the new climatic regime**, ISBN/ISSN: 978-0-7456-8433-8 978-0-7456-8434-5
- Matteo Meschiari,  **Geoanarchia: appunti di resistenza ecologica**, ISBN/ISSN: 978-88-99554-18-7


## Biology and Evolution
- Jan Sapp,  **The new foundations of evolution: on the tree of life**, ISBN/ISSN: 978-0-19-538849-7 978-0-19-538850-3
- Richard Levins, Richard Charles Lewontin,  **The dialectical biologist**, ISBN/ISSN: 978-0-674-20283-2
- Dorothy H Crawford,  **Deadly companions: how microbes shaped our history**, ISBN/ISSN: 978-0-19-255297-6 978-0-19-255298-3


## Biology and Microbes
- Cesar E. Giraldo Herrera,  **Microbes and other shamanic beings**, ISBN/ISSN: 978-3-319-71317-5
- Jan Sapp,  **The new foundations of evolution: on the tree of life**, ISBN/ISSN: 978-0-19-538849-7 978-0-19-538850-3
- Steve Mould,  **The Bacteria Book: The Big World of Really Tiny Microbes.**, ISBN/ISSN: 978-1-4654-7028-7
- Martin J Blaser,  **Missing microbes: how killing bacteria creates modern plagues**, ISBN/ISSN: 978-1-78074-688-3
- Idan Ben-Barak,  **Il regno invisibile: nel misterioso mondo dei microbi**, ISBN/ISSN: 978-88-220-6818-7
- Dorothy H Crawford,  **Deadly companions: how microbes shaped our history**, ISBN/ISSN: 978-0-19-255297-6 978-0-19-255298-3


## Climatic changes and Ecology
- Amitav Ghosh, Anna Nadotti, Norman Gobetti,  **La grande cecità: il cambiamento climatico e l'impensabile**, ISBN/ISSN: 978-88-545-1337-2
- David Wallace-Wells,  **La Terra inabitabile: una storia del futuro**, ISBN/ISSN: 978-88-04-71987-8
- Bruno Latour, Catherine Porter,  **Facing Gaia: eight lectures on the new climatic regime**, ISBN/ISSN: 978-0-7456-8433-8 978-0-7456-8434-5
- Marco Ferrari, Elisa Pasqual, Andrea Bagnato,  **A moving border: Alpine cartographies of climate change**, ISBN/ISSN: 978-1-941332-45-0


## Ecology and Sociology
- Déborah Danowski, Eduardo Batalha Viveiros de Castro,  **The ends of the world**, ISBN/ISSN: 978-1-5095-0397-1 978-1-5095-0398-8
- Gianni Celati,  **Verso la foce**, ISBN/ISSN: 978-88-07-81200-2
- Guido Viale,  **La parola ai rifiuti: scrittori e letture sull'aldilà delle merci**, ISBN/ISSN: 9788885747302


## Epistemology and Research methods
- Alex Reinhart,  **Statistics done wrong: the woefully complete guide**, ISBN/ISSN: 978-1-59327-620-1
- Henry M. Cowles,  **The scientific method: an evolution of thinking from Darwin to Dewey**, ISBN/ISSN: 978-0-674-97619-1
- Bruno Latour, Steve Woolgar,  **Laboratory life: the construction of scientific facts**, ISBN/ISSN: 978-0-691-02832-3 978-0-691-09418-2
- H. M. Collins,  **Artifictional intelligence: against humanity's surrender to computers**, ISBN/ISSN: 978-1-5095-0411-4 978-1-5095-0412-1


## Epistemology and Sociology
- Déborah Danowski, Eduardo Batalha Viveiros de Castro,  **The ends of the world**, ISBN/ISSN: 978-1-5095-0397-1 978-1-5095-0398-8
- Frank Pasquale,  **The black box society: the secret algorithms that control money and information**, ISBN/ISSN: 978-0-674-36827-9
- H. M. Collins,  **Artifictional intelligence: against humanity's surrender to computers**, ISBN/ISSN: 978-1-5095-0411-4 978-1-5095-0412-1
- Massimiano Bucchi,  **Science in society: an introduction to social studies of science**, ISBN/ISSN: 978-0-415-32199-0 978-0-415-32200-3


## Evolution and History
- Dorothy H Crawford,  **Deadly companions: how microbes shaped our history**, ISBN/ISSN: 978-0-19-255297-6 978-0-19-255298-3
- Richard Levins, Richard Charles Lewontin,  **The dialectical biologist**, ISBN/ISSN: 978-0-674-20283-2
- Henry M. Cowles,  **The scientific method: an evolution of thinking from Darwin to Dewey**, ISBN/ISSN: 978-0-674-97619-1


## Gravitation and Physics
- Peter Woit,  **Neanche sbagliata: il fallimento della teoria delle stringhe e la corsa all'unificazione delle leggi della fisica**, ISBN/ISSN: 978-88-7578-072-2
- H. M. Collins,  **Gravity's Ghost and Big Dog: scientific discovery and social analysis in the twenty-first century**, ISBN/ISSN: 978-0-226-05229-8
- Richard Phillips Feynman,  **Sei pezzi meno facili: relatività einsteiniana, simmetria, spazio-tempo**, ISBN/ISSN: 978-88-459-1870-4


## History and Research methods
- Richard Levins, Richard Charles Lewontin,  **The dialectical biologist**, ISBN/ISSN: 978-0-674-20283-2
- Andrea Wulf, Lapo Berti,  **L'invenzione della natura: le avventure di Alexander von Humboldt, l'eroe perduto della scienza**, ISBN/ISSN: 978-88-6105-262-8
- Henry M. Cowles,  **The scientific method: an evolution of thinking from Darwin to Dewey**, ISBN/ISSN: 978-0-674-97619-1
- Francesco Barreca,  **La scienza che fu: idee e strumenti di teorie abbandonate**, ISBN/ISSN: 978-88-7075-952-5


## Language and languages and Linguistics
- Andrea Moro,  **I confini di Babele: il cervello e il mistero delle lingue impossibili**, ISBN/ISSN: 978-88-15-27932-3
- Noam Chomsky,  **Il mistero del linguaggio: nuove prospettive**, ISBN/ISSN: 978-88-328-5032-1
- Maria Teresa Guasti,  **L' acquisizione del linguaggio: un'introduzione**, ISBN/ISSN: 978-88-6030-095-9


## Language and languages and Neurosciences
- Andrea Moro,  **I confini di Babele: il cervello e il mistero delle lingue impossibili**, ISBN/ISSN: 978-88-15-27932-3
- Stanislas Dehaene,  **How we learn: why brains learn better than any machine ... for now**, ISBN/ISSN: 978-0-525-55989-4
- Noam Chomsky,  **Il mistero del linguaggio: nuove prospettive**, ISBN/ISSN: 978-88-328-5032-1


## Medicine and Microbes
- David France,  **How to survive a plague: the inside story of how citizens and science tamed AIDS**, ISBN/ISSN: 9780307700636
- William Hall, Anthony McDonnell, Jim O'Neill,  **Superbugs: an arms race against bacteria**, ISBN/ISSN: 978-0-674-97598-9
- Alexander M. Nading,  **Mosquito trails: ecology, health, and the politics of entanglement**, ISBN/ISSN: 9780520282612 9780520282629
- Bruno Latour,  **The pasteurization of France**, ISBN/ISSN: 978-0-674-65761-8 978-0-674-65760-1


## Microbes and Research methods
- S. C Davies, Jonathan Grant, Mike Catchpole,  **The drugs don't work: a global threat**, ISBN/ISSN: 9780241969199
- David France,  **How to survive a plague: the inside story of how citizens and science tamed AIDS**, ISBN/ISSN: 9780307700636
- Agnes Ullmann,  **Origins of Molecular Biology: A Tribute to Jacques Monod**, ISBN/ISSN: 9781683672166 9781555817763 9781555812812


## Physics and Quantum
- Peter Woit,  **Neanche sbagliata: il fallimento della teoria delle stringhe e la corsa all'unificazione delle leggi della fisica**, ISBN/ISSN: 978-88-7578-072-2
- Terry Rudolph,  **Q is for quantum**, ISBN/ISSN: 978-0-9990635-0-7
- Sabine Hossenfelder,  **Lost in math: how beauty leads physics astray**, ISBN/ISSN: 978-0-465-09425-7


## Posthuman and Science fiction
- Philip K. Dick, C. Pagetti, R. Duranti,  **Ma gli androidi sognano pecore elettriche?**, ISBN/ISSN: 978-88-347-3888-7
- Nalo Hopkinson, Uppinder Mehan, Samuel R. Delany,  **So Long Been Dreaming: Postcolonial Science Fiction & Fantasy**, ISBN/ISSN: 978-1-55152-158-9
- Jeff VanderMeer, V. Latronico,  **Borne**, ISBN/ISSN: 978-88-06-23680-9




