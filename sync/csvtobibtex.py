import pandas as pd

class Entry:
	def __init__(self,series):
		self.series = series

		bibtex = {}

		bibex['title'] = self.series['Titolo']
		bibtex['author'] = self.series['Autori']
		bibtex['publisher'] = self.series['editore']
		bibtex['year'] = self.series['Data di pubblicazione'].split('/')[-1]
		bibtex['isbn'] = self.series['ISBN']
		bibtex['note'] = self.series['Trama']
		bibtex['pages'] = self.series['Pagine']
		bibtex['keywords'] = self.series['Categorie']
		bibtex['citekey']
		self.bibtex = bibtex

	def write_to_bibtex(self, filehandle):
		fh = filehandle
		template = """ @book{hawking1988,
title     = {},
author    = "Hawking, Stephen",
year      = 1988,
publisher = "Bantam",
address   = "London"
    }"""

		fw.write()


# def write_entry()

data  = pd.read_excel("data.xlsx")

print((data.loc[0]))